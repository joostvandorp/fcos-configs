.PHONY: help build fetch test login

# Build-time dependencies.
BUTANE ?= $(call find-cmd,butane)
PODMAN ?= $(call find-cmd,podman)
CURL   ?= $(call find-cmd,curl) $(if $(VERBOSE),,--progress-bar) --fail
GPG    ?= $(call find-cmd,gpg) $(if $(VERBOSE),,-q)
QEMU   ?= $(call find-cmd,qemu-system-x86_64) -enable-kvm
NC     ?= $(call find-cmd,nc) -vv -r -l


# Conditional command echo control.
VERBOSE :=

# CoreOS options.
STREAM := stable
LOCAL_VIRT_PATH := $(HOME)/.local/share/libvirt/images/
IMAGE := $(shell find $(LOCAL_VIRT_PATH) -name 'fedora-coreos*.qcow2' | head --lines=1)
IGNITION_CONFIG := $(shell realpath ./sandbox.ign)
RAM_MB := 2048


help:
	@echo "Usage: make [TARGET]"
	@echo ""
	@echo "Targets:"
	@echo "  help           Display this help message"
	@echo "  build          Build the ignition file"
	@echo "  test           Deploy the application in test mode"
	@echo "  fetch          Grabs latest CoreOS Image in qcow2 format"
	@echo "  login          When test successfully completes, use this to get shell"
	@echo "  dependencies   Install dependecies required to build butane files"

dependencies:
	sudo dnf install make butane coreos-installer ignition-validate


# Downloads the latest CoreOS installer image
fetch:
	mkdir -p ~/.local/share/libvirt/images/
	coreos-installer download -s "$(STREAM)" -p qemu -f qcow2.xz --decompress -C $(LOCAL_VIRT_PATH)


# Builds ignition file
build:
	rm -fr ./*.ign
	butane --strict butane/postgres.bu > butane/postgres.ign
	butane --strict butane/redis.bu > butane/redis.ign
	butane --strict butane/pihole.bu > butane/pihole.ign
	butane --strict butane/minio.bu > butane/minio.ign
	butane --strict butane/keycloak.bu > butane/keycloak.ign
	butane --strict --files-dir butane sandbox.bu > sandbox.ign
	ignition-validate sandbox.ign
	@ echo "Ignition file has been (re)built!"


# Runs the build command, then spins up a VM
test: build
	$Q $(QEMU) -m $(RAM_MB) -cpu host -snapshot \
	-fw_cfg name=opt/com.coreos/config,file=$(IGNITION_CONFIG) \
	-drive if=virtio,file=${IMAGE} \
	-nic user,model=virtio,hostfwd=tcp::2222-:22 \
	-daemonize


login:
	ssh -o "StrictHostKeyChecking=no" -p 2222 core@localhost


# Conditional command echo control.
Q := $(if $(VERBOSE),,@)

# Find and return full path to command by name, or throw error if none can be found in PATH.
# Example use: $(call find-cmd,ls)
find-cmd = $(or $(firstword $(wildcard $(addsuffix /$(1),$(subst :, ,$(PATH))))),$(error "Command '$(1)' not found in PATH"))

# Shell colors, used in messages.
BOLD      = \033[1m
UNDERLINE = \033[4m
BLUE      = \033[36m
RESET     = \033[0m

# Variables for reserved characters.
SPACE = $(eval) $(eval)
COMMA = ,
