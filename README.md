# FCOS Configs

```
butane --pretty --strict --files-dir butane sandbox.bu > sandbox.ign
```

## install FCOS

```
sudo coreos-installer install /dev/sda \
    --ignition-url https://gitlab.com/joostvandorp/fcos-configs/-/raw/main/sandbox.ign
```


## Makefile

```
make test
```
This command should build and launch coreOS using QEMU. 


```
make login
```
This will give shell, if you have the right private key on your system.
If your private key is different, change it in the main sandbox.bu file

```
passwd:
  users:
    - name: core
      ssh_authorized_keys:
```